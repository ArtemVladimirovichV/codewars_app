class ApplicationController < ActionController::Base
  helper_method :current_user
  
  private 

  def current_user
    @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id].present?
  end

  def user_sign_in?
    current_user.present?
  end

  def sign_in(user)
    session[:user_id] = user.id
  end

  def flash_success?(flash)
    flash[:success].present?
  end

  def flash_alert?(flash)
    flash[:alert].present?
  end

  def alerts(flash)
    (flash[:alert].kind_of?(String) ? [flash[:alert]] : flash[:alert])
  end

  def admin_sign_in?
    current_user.present? && current_user[:nickname] == 'admin'
  end

  def require_no_authentication
    return if !user_sign_in?

      flash[:alert] = 'You are already signed in'
      redirect_to root_path
  end

  def require_authentication
    return if user_sign_in?

    flash[:warning] = 'You are not signed in'
    redirect_to root_path
  end

  def tests
    Constant::TESTS
  end

  def exam(number)
    if (tests.keys.min.to_s.to_i < number || tests.keys.min.to_s.to_i == number)
      service_tests_numbers = tests.select{|k, v| k.to_s.to_i <= @completed_solutions.count}
      @service_tests = Service.where(id: (service_tests_numbers.values)).order('id DESC')
    else
      @service_tests = "You don't have enough completed tasks"
    end

    @service_tests
  end
  
  helper_method :user_sign_in?, :flash_success?, :flash_alert?, :alerts, :admin_sign_in, :exam, :tests
end
