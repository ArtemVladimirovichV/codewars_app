class SessionsController < ApplicationController
  before_action :require_no_authentication, only: %i[new create]
  before_action :require_authentication, only: :destroy

  def new; end

  def create
    user = User.find_by(email: user_params[:email])
    if user&.authenticate(user_params[:password])
      sign_in user
      flash[:success] = "#{user.name}, you have successfully logged in!"
      
      redirect_to root_path
    else
      flash.now[:alert] = 'Wrong email or password'
      render :new
    end
  end

  def destroy
    session.delete(:user_id)
    flash[:alert] = 'You are logged out'
    redirect_to root_path
  end

  private

  def user_params
    params[:session]
  end
end
