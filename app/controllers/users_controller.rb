class UsersController < ApplicationController
  before_action :require_no_authentication, only: %i[new create]
  
  def new
    session[:current_time] = Time.now
    @user = User.new
  end

  def create
    @user = User.create user_params
    if @user.valid?
      sign_in @user
      flash[:success] = "Welcome to the app, #{@user.name}!"
      redirect_to root_path
    else      
      flash[:alert] = @user.errors.full_messages      
      redirect_to new_user_path
    end
  end

  def show
    find_user unless current_user.nil?
    redirect_to services_path
  end

  def destroy
    if admin_sign_in?
      @user_destroy = User.find_by(id: params[:id])
      @user_destroy.destroy
      redirect_to users_path
    else
      flash[:alert] = "This functionality is available only to the administrator"
      redirect_to root_path
    end
  end

  private

  def find_user
    @user = User.find(current_user[:id])
  end

  def user_params
    params.require(:user).permit(:name, :nickname, :email, :password, :password_confirmation)
  end
end
