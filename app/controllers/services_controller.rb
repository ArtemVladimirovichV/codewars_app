class ServicesController < ApplicationController
  before_action :find_service, only: %i[show update edit destroy]
  helper_method :sort_column, :sort_direction

  def new
    @service = Service.new
  end

  def index
    if current_user.present?
      @completed_solutions = (
        current_user.solutions_numbers.blank? ? 
        "You don't have completed tasks yet" : 
        Service.where(id: JSON.parse(current_user.solutions_numbers)).order('id DESC')
      )

      @service_tests = (
        @completed_solutions.kind_of?(String) ?
        "You don't have enough completed tasks" : exam(@completed_solutions&.count)
      )
    end
    @services = Service.where.not(id: (tests.values)).order(sort_column + " " + sort_direction).page params[:page]
  end

  def show
    @test_input = params[:test_input]
    @result = params[:result]
  end

  def update
    unless params[:service][:test_input].nil?
      @result = @service.service_class_name.constantize.new(params[:service][:test_input]).calculate_result

      Add::Base.new(current_user).add_solution(params[:id]) if current_user.present?

      redirect_to(service_path(id: params[:id], test_input: params[:service][:test_input], result: @result))
    else
      if admin_sign_in?
        if @service.update service_params
          flash.now[:success] = "#{@service.title} kata has been successfully updated!"
          redirect_to service_path(params[:service][:id])
        else
          flash[:alert] = @service.errors.full_messages
          render :edit
        end
      else
        flash[:alert] = "This functionality is available only to the administrator"
        redirect_to services_path
      end
    end
  end

  def edit; end

  def create
    if admin_sign_in?
      @service = Service.create(service_params)
      if @service.valid?
        flash[:success] = "#{@service.title} kata successfully created!"
        redirect_to services_path
      else
        flash[:alert] = @service.errors.full_messages
        redirect_to new_service_path
      end
    else
      flash[:alert] = "This functionality is available only to the administrator"
      redirect_to services_path
    end
  end

  def destroy
    if admin_sign_in?
      @service.destroy
      flash[:alert] = "You removed the #{@service.title} kata"
      redirect_to services_path
    else
      flash[:alert] = "This functionality is available only to the administrator"
      redirect_to services_path
    end
  end

  private

  def find_service
    @service = Service.find(params[:id])
  end

  def sort_column
    Service.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def service_params
    params.require(:service).permit(:id, :rank, :title, :link, :description, :solution_example, :service_class_name)
  end
end
