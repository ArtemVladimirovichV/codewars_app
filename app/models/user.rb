class User < ApplicationRecord
  has_secure_password

  validates :name, presence: true, length: { minimum: 2 }
  validates :email, presence: true, uniqueness: true
  validates :nickname, presence: true, length: { minimum: 3 }, uniqueness: true
  validate :password_complexity
  validates :password_digest, presence: true

  private
  
  def password_complexity
    return if password.blank? || password =~ /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9;\/\|\\\&\$\#\_\-]{8,20}/

    msg = "complexity requirement not met." \
          "Length should be 8-20 characters and include at least:" \
          "1 uppercase, 1 lowercase, 1 digit and it can include 'special characters like (\/\|\\\&\$\#\_\-)"
    errors.add :password, msg
  end
end
