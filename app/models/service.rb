class Service < ApplicationRecord
  validates :id, presence: true, uniqueness: true, numericality: { only_integer: true }
  validates :rank, presence: true, numericality: { only_integer: true }
  validates :title, presence: true, length: { minimum: 4 }
  validates :link, presence: true, uniqueness: true, format: { with: /\Ahttps:\/\/www.codewars.com\/kata\//, message: "only links from Codewards" }
  validates :description, presence: true, length: { minimum: 20 }
  validates :service_class_name, presence: true, uniqueness: true, length: { minimum: 6 },format: { with: /::Base\z/, message: "must end with '::Base'" }
end