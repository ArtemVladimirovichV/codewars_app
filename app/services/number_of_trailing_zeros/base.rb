module NumberOfTrailingZeros
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      res = 0
      res += @n /= 5 while @n >= 1
      res
    end
  end
end
