module ExtractTheDomainNameFromUrl
  class Base
    def initialize(url)
      @url = url
    end

    def calculate_result
      (@url.gsub!(/(w{3}\.)?(https?:\/\/)?/, '').match /[\w_-]*?\./)[0][0...-1]
    end
  end
end
