module IsbnValidation
  class Base
    def initialize(isbn)
      @isbn = isbn.to_s
    end

    def calculate_result
      if @isbn.match(/^[0-9]{9}[0-9X]/) && @isbn.size == 10
        num = 0
        range = 1
        @isbn[0...-1].each_char do |item|
          num += item.to_i * range.to_i
          range += 1
        end
        num += @isbn[-1].match(/X/) ? 10 * 10 : @isbn[-1].to_i * 10
        (num % 11).zero? ? true : false
      else
        false
      end
    end
  end
end
