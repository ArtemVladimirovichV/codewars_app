require 'json'

module TakeATenMinutesWalk
  class Base
    def initialize(string)
      string.gsub!("'", '"')
      @string = JSON.parse(string)
    end

    def calculate_result
      return false if @string.size != 10

      n = @string.find_all { |i| i.to_s == 'n' }
      s = @string.find_all { |i| i.to_s == 's' }
      w = @string.find_all { |i| i.to_s == 'w' }
      e = @string.find_all { |i| i.to_s == 'e' }

      (n.size == s.size && e.size == w.size) ? true : false
    end
  end
end
