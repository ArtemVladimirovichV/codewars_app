require 'json'

module TribonacciSequence
  class Base
    def initialize(conditions)
      @conditions = conditions.split("], ")
      @array = JSON.parse(@conditions[0] << "]")
      @n = @conditions[1].to_i
    end

    def calculate_result
      array = [@array[0], @array[1], @array[2]]
      (4..@n).each_with_index {|item, index| array << (num = [array[index], array[index + 1], array[index + 2]].sum)}
  
      array.first(@n).to_s
    end
  end
end
