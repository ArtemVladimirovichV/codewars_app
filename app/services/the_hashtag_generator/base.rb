module TheHashtagGenerator
  class Base
    def initialize(str)
      @str = str
    end

    def calculate_result
      a = @str.split.map(&:capitalize)
      if a.empty? || a.join('').size > 139
        false
      else
        "##{a.join('')}"
      end
    end
  end
end
