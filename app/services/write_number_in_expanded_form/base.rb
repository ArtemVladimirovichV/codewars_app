module WriteNumberInExpandedForm
  class Base
    def initialize(string)
      @string = string.to_i
    end

    def calculate_result
      n = ''
      a = @string.digits.reverse
      range = a.size

      a.each do |item|
        range -= 1
        unless item.zero?
          n += range == a.size - 1 ? '' : ' + '
          n += (item.to_i * 10**range.to_i).to_s.to_s
        end
      end
      n
    end
  end
end
