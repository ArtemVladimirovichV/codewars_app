module SumOddNumbers
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      @n**3
    end
  end
end
