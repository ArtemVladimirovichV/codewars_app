module StringIncrementer
  class Base
    def initialize(string)
      @string = string
    end

    def calculate_result
      m = (@string.match /[0-9]*$/)[0]
      b = m.empty? ? @string << '1' : (@string[0...-m.size] + "%0#{m.size}d" % (m.to_i + 1))
    end
  end
end
