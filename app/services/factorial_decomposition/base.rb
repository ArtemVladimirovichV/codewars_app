require 'prime'

module FactorialDecomposition
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      (1..@n).inject(1) do |result, item|
        result * item
      end.prime_division.map { |prime, range| range == 1 ? prime.to_s : "#{prime}^#{range}" }.join(' * ')
    end
  end
end
