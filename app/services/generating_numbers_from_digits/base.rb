require 'json'

module GeneratingNumbersFromDigits
  class Base
    def initialize(arr)
      arr.gsub!("'", '"')
      @arr = JSON.parse(arr)
    end

    def calculate_result
      min = @arr.sort.join.to_i
      max = @arr.sort.reverse.join.to_i
      option = factorial(@arr.size) / len(@arr)

      [option, min, max]
    end

    def factorial(n)
      (1..n).inject(1) { |result, item| result * item }
    end

    def len(array)
      array.uniq.inject(1) { |r, i| r * factorial(array.count(i)) }
    end
  end
end
