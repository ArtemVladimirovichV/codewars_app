module CalculatePyramidHeight
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      floor = 0

      while @n.positive?
        floor += 1
        @n -= (floor * floor)
      end

      floor -= 1 if @n.negative?
      floor
    end
  end
end
