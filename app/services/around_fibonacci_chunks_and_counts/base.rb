module AroundFibonacciChunksAndCounts
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      arr = (@n - 1).times.each_with_object([0, 1]) { |_item, obj| obj << obj[-2] + obj[-1] }

      a = arr.last.to_s
      ch = (a.size % 25).zero? ? a[-25..] : a[-(a.size % 25)..]
      maxcnt = 0
      maxitem = ''
      a.split('').sort.uniq.each do |b|
        if maxcnt < a.count(b)
          maxcnt = a.count(b)
          maxitem = b
        end
      end
      "Last chunk #{ch}; Max is #{maxcnt} for digit #{maxitem}"
    end
  end
end
