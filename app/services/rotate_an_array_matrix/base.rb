require 'json'

module RotateAnArrayMatrix
  class Base
    def initialize(arrays)
      @conditions = arrays.split("]], ")
      @matrix = JSON.parse(@conditions[0] << "]]")
      @direction = @conditions[1]
    end

    def calculate_result
      @direction == 'clockwise' ? @matrix.reverse.transpose.to_s : matrix.transpose.reverse.to_s
    end
  end
end
