require 'json'

module Add
  class Base

    def initialize(current_user)
      @user = current_user
    end

    def add_solution(id)
      @user.update(solutions_numbers: add_solution_number(id))
    end

    private

    def add_solution_number(id)
      @user[:solutions_numbers] = "[]" if (@user[:solutions_numbers]&.blank? || @user[:solutions_numbers].nil?)
      (JSON.parse(@user[:solutions_numbers]) << id.to_i).uniq
    end
  end
end