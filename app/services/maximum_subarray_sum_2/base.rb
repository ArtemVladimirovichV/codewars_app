require 'json'

module MaximumSubarraySum2
  class Base
    def initialize(arr)
      @arr = JSON.parse(arr)
    end

    def calculate_result
      max_arr = []; sub_arr = []
      max = 0
      @arr.each do |n|
        sub_arr << n
        if max < sub_arr.sum
          max_arr = [sub_arr.dup]
          max = sub_arr.sum
        elsif max == sub_arr.sum
          max_arr << sub_arr.dup
        end
        sub_arr = [] if sub_arr.sum.negative?
      end
      [max_arr.size == 1 ? max_arr[0] : max_arr, max]
    end
  end
end
