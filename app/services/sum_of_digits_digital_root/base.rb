module SumOfDigitsDigitalRoot
  class Base
    def initialize(num)
      @n = num.to_i
    end

    def calculate_result
      return 0 if @n.zero?

      while @n.to_s.length != 1
        sum = 0
        @n.digits.each { |num| sum += num }
        @n = sum
      end
      @n
    end
  end
end
