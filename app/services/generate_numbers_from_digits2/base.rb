module GenerateNumbersFromDigits2
  class Base
    def initialize(arr)
      arr.gsub!("'", '"')
      @arr = JSON.parse(arr)
    end

    def calculate_result
      z = @arr.uniq
      n = @arr.uniq.map { |item| @arr.count(item) }
      max = @arr.uniq.sort.reverse[0...(n.min)].join.to_i
      a = n.size**n.min

      b = a - (z.size + 1 - n.min..z.size).reduce(:*)

      (max.to_s.size == n.min ? [a, b, max] : [a]).to_s
    end
  end
end
