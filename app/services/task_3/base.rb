module Task3
  class Base
    def initialize(str)
      @str = str
    end

    def calculate_result
      dot_product([1, 1, 1], [2, -2, 2])
    end

    def dot_product(a, b)
      eval(@str)
    end
  end
end