module NumbersThatAreAPowerOfTheirSumOfDigits
  class Base
    def initialize(number)
      @number = number.to_i
    end

    def calculate_result
      arr = [81]
      (7..150).each do |num|
        (3..15).each do |m|
          x = num**m
          a = x.digits.sum
          arr << x if a == num
        end
      end
      arr.sort[@number - 1]
    end
  end
end
