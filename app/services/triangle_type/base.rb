module TriangleType
  class Base
    def initialize(sides)
      @sides = sides.split(', ')
      @a = @sides[0].to_i; @b = @sides[1].to_i; @c = @sides[2].to_i
    end

    def calculate_result
      side = [@a, @b, @c].sort!
      sum = side[0]**2 + side[1]**2
      hyp = side[2]**2
      ((@a >= @b + @c) || (@b >= @a + @c) || (@c >= @a + @b)) ? 0 : ((hyp < sum) ? 1 : ((hyp == sum) ? 2 : 3 ))
    end
  end
end
