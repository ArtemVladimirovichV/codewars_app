module RectangleIntoSquares
  class Base
    def initialize(string)
      @string = string.to_s.split(', ')
      @lng = @string[0].to_i
      @wdth = @string[1].to_i
    end

    def calculate_result
      if @lng == @wdth
        nil
      else
        arr = []
        while @lng != @wdth
          if @lng > @wdth
            arr << @wdth
            @lng -= @wdth
          else
            arr << @lng
            @wdth -= @lng
          end
        end
        arr << @lng
      end
    end
  end
end
