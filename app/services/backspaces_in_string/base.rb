module BackspacesInString
  class Base
    def initialize(string)
      @string = string.to_s
    end

    def calculate_result
      while @string.include?('#')
        i = @string.index('#')
        @string.slice!(i)
        @string.slice!(i - 1) if i != 0
      end
      @string
    end
  end
end
