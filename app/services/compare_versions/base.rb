module CompareVersions
  class Base
    def initialize(string)
      @string = string.split(', ')
    end

    def calculate_result
      Gem::Version.new(@string[0]) >= Gem::Version.new(@string[1])
    end
  end
end
