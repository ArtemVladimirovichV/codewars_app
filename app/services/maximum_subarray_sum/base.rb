require 'json'

module MaximumSubarraySum
  class Base
    def initialize(arr)
      @arr = JSON.parse(arr)
    end

    def calculate_result
      max = 0
      sum = 0
      @arr.each do |num|
        sum += num
        sum = 0 if sum.negative?
        max = sum if sum > max
      end
      max
    end
  end
end
