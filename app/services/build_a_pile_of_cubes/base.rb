module BuildAPileOfCubes
  class Base
    def initialize(num)
      @num = num.to_i
    end

    def calculate_result
      i = 0; n = 1
      while @num != 0
        return -1 if @num.negative?

        @num -= n.pow(3)
        i += 1; n += 1
      end
      i
    end
  end
end
