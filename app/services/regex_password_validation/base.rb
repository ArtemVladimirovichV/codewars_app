module RegexPasswordValidation
  class Base
    def initialize(regex)
      @regex = regex
    end

    def calculate_result
      @regex.match?(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{6,}$/)
    end
  end
end
