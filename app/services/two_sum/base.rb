module TwoSum
  class Base
    def initialize(string)
      @string = string.delete('[').split('], ')
      @numbers = (@string[0]).split(', ')
      @target = @string[1].to_i
    end

    def calculate_result
      arr = []

      @numbers.each_with_index do |i, iindex|
        @numbers.each_with_index do |j, jindex|
          if (i.to_i + j.to_i == @target) && (iindex != jindex)
            arr << iindex
            arr << jindex
          end
        end
      end

      arr.uniq
    end
  end
end
