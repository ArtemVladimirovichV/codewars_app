module DoesMyNumberLookBigInThis
  class Base
    def initialize(string)
      @string = string.to_i
    end

    def calculate_result
      sum = 0
      range = @string.digits.size
      @string.digits.each do |x|
        sum += x**range
      end
      sum == @string
    end
  end
end
