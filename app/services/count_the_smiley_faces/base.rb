require 'json'

module CountTheSmileyFaces
  class Base
    def initialize(string)
      @string = JSON.parse(string)
    end

    def calculate_result
      count = 0
      @string.each do |i|
        count += 1 if /[:;][~-]?[)D]/.match(i)
      end
      count.to_s
    end
  end
end
