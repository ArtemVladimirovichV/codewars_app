module MakeUpperCase
  class Base
    def initialize(str)
      @str = str
    end

    def calculate_result
      @str.upcase
    end
  end
end
