module StringsMix
  class Base
    def initialize(url)
      @url = url
    end

    def calculate_result
      kl
    end
  end
end
module StringsMix
  class Base
    def initialize(strings)
      @strings = strings.split('", "')
      @s1 = @strings[0]
      @s2 = @strings[1]
    end

    def calculate_result
      a = []
      first = transform(@s2 + @s1)
      first.each {|letter| a << (
        @s1.count(letter) == @s2.count(letter) ? "=:#{letter * @s1.count(letter)}" : (
          @s1.count(letter) > @s2.count(letter) ? "1:#{letter * @s1.count(letter)}" : 
          "2:#{letter * @s2.count(letter)}")) if (@s1.count(letter) > 1 || @s2.count(letter) > 1)
      }
      
      a.sort_by { |x| [-x.size, x[0], x[-1]] }.join('/')
    end

    def transform(str)
      str.downcase.split('').select {|i| i.match(/[a-z]/)}.uniq
    end
  end
end