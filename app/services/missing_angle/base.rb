include Math

module MissingAngle
  class Base
    def initialize(num)
      @num = num.split(', ')
      @h = @num[0].to_i
      @a = @num[1].to_i
      @o = @num[2].to_i
    end

    def calculate_result
      @a = (@a == 0 ? (sqrt(@h**2 - @o**2)) : @a)
      @h = (@h == 0 ? (sqrt(@a**2 + @o**2)) : @h)
      angle = (@a / @h.to_f)
      ((acos(angle)) * 180 / PI).round
    end
  end
end
