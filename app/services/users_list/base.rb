module UsersList
  class Base
    def initialize(current_user)
      @user = current_user
    end

    def show_all_users
      @users = User.where.not(nickname: 'admin', email: 'mr.vitko@mail.ru') if (@user[:nickname] == 'admin' && @user[:email] == 'mr.vitko@mail.ru')
    end
  end
end
