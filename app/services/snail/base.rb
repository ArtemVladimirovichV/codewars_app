require 'json'

module Snail
  class Base
    def initialize(array)
      @array = JSON.parse(array)
    end

    def calculate_result
      snail(@array).to_s
    end

    def snail(array)
      result = array.empty? ? [] : array.shift + snail(array.transpose.reverse)
    end
  end
end
