require 'json'

module FindTheUniqueNumber
  class Base
    def initialize(arr)
      @arr = JSON.parse(arr)
    end

    def calculate_result
      @arr.delete(@arr[0]) if @arr.count(@arr[0].to_i) != 1
      @arr[0]
    end
  end
end
