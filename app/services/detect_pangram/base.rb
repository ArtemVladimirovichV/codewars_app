module DetectPangram
  class Base
    def initialize(string)
      @string = string
    end

    def calculate_result
      @string.downcase.scan(/[a-z]/).uniq.size == 26
    end
  end
end
