require 'json'

module RangeExtraction
  class Base
    def initialize(list)
      @list = JSON.parse(list)
    end

    def calculate_result
      arr = []; a = []
      @list[0..-1].each_with_index { |item, index|
        if (item + 1 == @list[index+1])
          arr << @list[index]
        elsif ((item - 1 == @list[index-1]))
          arr << @list[index]
          arr.size > 2 ? (a << "#{arr.min}-#{arr.max}") : (a << arr.min; a << arr.max)
          arr.clear
        else
          a << @list[index]
        end
      }
      a.join(',')
    end
  end
end
