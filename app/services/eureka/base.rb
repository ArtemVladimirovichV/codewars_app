module Eureka
  class Base
    def initialize(str)
      @str = str.split(', ')
    end

    def calculate_result
      array = []
      (@str[0].to_i..@str[1].to_i).each do |i|
        n = 0
        a = i.digits.reverse
        a.each_with_index do |item, index|
          n += item.to_i**(index + 1).to_i
        end
        array << i if i == n
      end
      array
    end
  end
end
