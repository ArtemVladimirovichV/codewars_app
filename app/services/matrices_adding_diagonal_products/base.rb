require 'json'

module MatricesAddingDiagonalProducts
  class Base
    def initialize(matrix)
      @matrix = JSON.parse(matrix)
    end

    def calculate_result
      n = @matrix.size
      matrix_back = []
      @matrix.each {|el| matrix_back << el.reverse}

      sum1 = back_sum(@matrix, n) + main_sum(@matrix, n)
      sum2 = back_sum(matrix_back, n) + main_sum(matrix_back, n)
      result = (sum1 - sum2).to_s
    end

    def back_sum(matrix, n)
      sum = 0
      (n - 1).times do |el|
        el += 1
        res = (0...(n - el)).inject(1) {|result, i| result * (matrix[i + el][i]) }
        res1 = (0...(n - el)).inject(1) {|result, i| result * (matrix[i][i + el]) }
        sum += res + res1
      end  
      sum
    end

    def main_sum(matrix, n)
      res = (0...n).inject(1) {|result, i| result * (matrix[i][i])}
    end
  end
end
