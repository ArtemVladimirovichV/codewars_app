require 'prime'

module BuddyPairs
  class Base
    def initialize(string)
      @string = string.to_s.split(', ')
      @start = @string[0]
      @limit = @string[1]
    end

    def calculate_result
      (@start..@limit).each do |n|
        n = n.to_i
        m = s(n) - 1
        next unless m >= n
        return "(#{n} #{m})" if s(m) == n + 1
      end
      'Nothing'
    end

    def s(n)
      n.prime_division.inject(1) { |a, (f, p)| a * (f**(p + 1) - 1) / (f - 1) } - n
    end
  end
end
