require 'json'

module RankVector
  class Base
    def initialize(string)
      @string = JSON.parse(string)
    end

    def calculate_result
      b = @string.sort { |x, y| y <=> x }
      arr = []
      @string.each do |item|
        arr << (b.index(item) + 1)
      end
      arr.to_s
    end
  end
end
