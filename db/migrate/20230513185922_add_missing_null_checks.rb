class AddMissingNullChecks < ActiveRecord::Migration[7.0]
  def change
    def change
      change_column_null :services, :rank, false
      change_column_null :services, :title, false
      change_column_null :services, :link, false
      change_column_null :services, :description, false
      change_column_null :services, :service_class_name, false
      change_column_null :services, :solution_example, false
  
      change_column_null :users, :name, false
      change_column_null :users, :nickname, false
      change_column_null :users, :email, false
      change_column_null :users, :password_digest, false
      change_column_null :users, :solutions_numbers, false
    end
  end
end
