class AddSolutionExampleToServices < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :solution_example, :string, default: "", null: false
  end
end
