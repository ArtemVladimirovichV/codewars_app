class AddSolutionsNumbersToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :solutions_numbers, :string, default: "", null: false
  end
end
