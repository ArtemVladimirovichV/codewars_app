class CreateServices < ActiveRecord::Migration[7.0]
  def change
    create_table :services do |t|
      t.integer :rank
      t.string :title
      t.string :link
      t.string :description
      t.string :service_class_name

      t.timestamps
    end
  end
end
