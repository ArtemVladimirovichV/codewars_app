Rails.application.routes.draw do
  root to: 'users#show'

  resources :services
  resource :session, only: %i[new create destroy]
  resources :users, only: %i[show new create destroy]
end
