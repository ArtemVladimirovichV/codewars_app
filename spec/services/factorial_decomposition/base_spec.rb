require 'rails_helper'

RSpec.describe 'FactorialDecomposition:Base' do
  before do
    @number = '5'
    @answer = '2^3 * 3 * 5'
  end

  it 'should get result' do
    expect(FactorialDecomposition::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
