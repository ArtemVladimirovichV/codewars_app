require 'rails_helper'

RSpec.describe 'BuddyPairs:Base' do
  before do
    @numbers = '1071625, 1103735'
    @answer = '(1081184 1331967)'
  end

  it 'should get result' do
    expect(BuddyPairs::Base.new(@numbers).calculate_result).to eq(@answer)
  end
end
