require 'rails_helper'

RSpec.describe 'MaximumSubarraySum2:Base' do
  before do
    @array = '[4, -1, 2, 1, -40, 1, 2, -1, 4]'
    @answer = '[[[4, -1, 2, 1], [1, 2, -1, 4]], 6]'
  end

  it 'should get result' do
    expect(MaximumSubarraySum2::Base.new(@array).calculate_result.to_s).to eq(@answer)
  end
end
