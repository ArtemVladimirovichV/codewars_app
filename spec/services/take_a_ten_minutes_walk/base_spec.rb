require 'rails_helper'

RSpec.describe 'TakeATenMinutesWalk:Base' do
  before do
    @condition = "['n','s','n','s','n','s','n','s','n','s']"
    @answer = true
  end

  it 'should get result' do
    expect(TakeATenMinutesWalk::Base.new(@condition).calculate_result).to eq(@answer)
  end
end
