require 'rails_helper'

RSpec.describe 'RectangleIntoSquares:Base' do
  before do
    @condition = '5, 3'
    @answer = [3, 2, 1, 1]
  end

  it 'should get result' do
    expect(RectangleIntoSquares::Base.new(@condition).calculate_result).to eq(@answer)
  end
end
