require 'rails_helper'

RSpec.describe 'CompareVersions:Base' do
  before do
    @letters = '10.4, 10.10'
    @answers = false
  end

  it 'should get result' do
    expect(CompareVersions::Base.new(@letters).calculate_result).to eq(@answers)
  end
end
