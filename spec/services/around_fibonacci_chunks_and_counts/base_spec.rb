require 'rails_helper'

RSpec.describe 'AroundFibonacciChunksAndCounts:Base' do
  before do
    @number = '100'
    @answer = 'Last chunk 354224848179261915075; Max is 3 for digit 1'
  end

  it 'should get result' do
    expect(AroundFibonacciChunksAndCounts::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
