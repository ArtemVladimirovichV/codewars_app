require 'rails_helper'

RSpec.describe 'RotateAnArrayMatrix:Base' do
  before do
    @conditions = "[[1, 2, 3], [4, 5, 6], [7, 8, 9]], clockwise"
    @answer = "[[7, 4, 1], [8, 5, 2], [9, 6, 3]]"
  end

  it 'should get result' do
    expect(RotateAnArrayMatrix::Base.new(@conditions).calculate_result).to eq(@answer)
  end
end
