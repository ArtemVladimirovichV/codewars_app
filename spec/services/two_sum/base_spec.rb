require 'rails_helper'

RSpec.describe 'TwoSum:Base' do
  before do
    @numbers = '[1, 2, 3], 4'
    @answers = [0, 2]
  end

  it 'should get result' do
    expect(TwoSum::Base.new(@numbers).calculate_result).to eq(@answers)
  end
end
