require 'rails_helper'

RSpec.describe 'Function1HelloWorld:Base' do
  before do
    @answer = "hello world!"
  end

  it 'should get result' do
    expect(Function1HelloWorld::Base.new().calculate_result.to_s).to eq(@answer)
  end
end
