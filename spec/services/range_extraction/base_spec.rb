require 'rails_helper'

RSpec.describe 'RangeExtraction:Base' do
  before do
    @string = "[-6, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20]"
    @answer = "-6,-3-1,3-5,7-11,14,15,17-20"
  end

  it 'should get result' do
    expect(RangeExtraction::Base.new(@string).calculate_result).to eq(@answer)
  end
end
