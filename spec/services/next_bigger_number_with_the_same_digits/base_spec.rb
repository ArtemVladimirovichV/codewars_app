require 'rails_helper'

RSpec.describe 'NextBiggerNumberWithTheSameDigits:Base' do
  before do
    @number = "2017"
    @answer = "2071"
  end

  it 'should get result' do
    expect(NextBiggerNumberWithTheSameDigits::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
