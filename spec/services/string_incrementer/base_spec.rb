require 'rails_helper'

RSpec.describe 'StringIncrementer:Base' do
  before do
    @string = "foobar99"
    @answer = "foobar100"
  end

  it 'should get result' do
    expect(StringIncrementer::Base.new(@string).calculate_result).to eq(@answer)
  end
end
