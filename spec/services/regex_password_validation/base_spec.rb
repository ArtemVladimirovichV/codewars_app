require 'rails_helper'

RSpec.describe 'RegexPasswordValidation:Base' do
  before do
    @string = "djI38D55"
    @answer = true
  end

  it 'should get result' do
    expect(RegexPasswordValidation::Base.new(@string).calculate_result).to eq(@answer)
  end
end
