require 'rails_helper'

RSpec.describe 'TheHashtagGenerator:Base' do
  before do
    @string = 'Codewars is nice'
    @answer = '#CodewarsIsNice'
  end

  it 'should get result' do
    expect(TheHashtagGenerator::Base.new(@string).calculate_result).to eq(@answer)
  end
end
