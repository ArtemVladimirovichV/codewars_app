require 'rails_helper'

RSpec.describe 'Snail:Base' do
  before do
    @array = "[[1, 2, 3],[4, 5, 6],[7, 8, 9]]"
    @answer = "[1, 2, 3, 6, 9, 8, 7, 4, 5]"
  end

  it 'should get result' do
    expect(Snail::Base.new(@array).calculate_result).to eq(@answer)
  end
end
