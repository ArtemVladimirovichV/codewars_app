require 'rails_helper'

RSpec.describe 'DetectPangram:Base' do
  before do
    @condition = 'The quick brown fox jumps over the lazy dog.'
    @answer = true
  end

  it 'should get result' do
    expect(DetectPangram::Base.new(@condition).calculate_result).to eq(@answer)
  end
end
