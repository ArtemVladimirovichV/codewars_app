require 'rails_helper'

RSpec.describe 'BuildAPileOfCubes:Base' do
  before do
    @condition = '4183059834009'
    @answer = '2022'
  end

  it 'should get result' do
    expect(BuildAPileOfCubes::Base.new(@condition).calculate_result).to eq(@answer.to_i)
  end
end
