require 'rails_helper'

RSpec.describe 'IsbnValidation:Base' do
  before do
    @number = 'X123456788'
    @answer = 'false'
  end

  it 'should get result' do
    expect(IsbnValidation::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
