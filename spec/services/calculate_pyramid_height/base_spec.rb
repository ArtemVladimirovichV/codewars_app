require 'rails_helper'

RSpec.describe 'CalculatePyramidHeight:Base' do
  before do
    @number = '1240'
    @answer = '15'
  end

  it 'should get result' do
    expect(CalculatePyramidHeight::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
