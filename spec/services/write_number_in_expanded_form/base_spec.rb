require 'rails_helper'

RSpec.describe 'WriteNumberInExpandedForm:Base' do
  before do
    @number = '12'
    @answers = '10 + 2'
  end

  it 'should get result' do
    expect(WriteNumberInExpandedForm::Base.new(@number.to_i).calculate_result).to eq(@answers)
  end
end
