require 'rails_helper'

RSpec.describe 'SumOddNumbers:Base' do
  before do
    @number = '2'
    @answers = '8'
  end

  it 'should get result' do
    expect(SumOddNumbers::Base.new(@number.to_i).calculate_result).to eq(@answers.to_i)
  end
end
