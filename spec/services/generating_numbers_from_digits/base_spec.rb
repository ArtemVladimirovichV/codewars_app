require 'rails_helper'

RSpec.describe 'GeneratingNumbersFromDigits:Base' do
  before do
    @array = "['1','2','3','0','5','1','1','3']"
    @answer = '[3360, 1112335, 53321110]'
  end

  it 'should get result' do
    expect(GeneratingNumbersFromDigits::Base.new(@array).calculate_result.to_s).to eq(@answer)
  end
end
