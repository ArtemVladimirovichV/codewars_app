require 'rails_helper'

RSpec.describe 'MatricesAddingDiagonalProducts:Base' do
  before do
    @matrix = "[[ 1,  4, 7,  6,  5],
                [-3,  2, 8,  1,  3],
                [ 6,  2, 9,  7, -4],
                [ 1, -2, 4, -2,  6],
                [ 3,  2, 2, -4,  7]]"
    @answer = "1098"
  end

  it 'should get result' do
    expect(MatricesAddingDiagonalProducts::Base.new(@matrix).calculate_result).to eq(@answer)
  end
end
