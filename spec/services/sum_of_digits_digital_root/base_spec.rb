require 'rails_helper'

RSpec.describe 'SumOfDigitsDigitalRoot:Base' do
  before do
    @number = '942'
    @answer = '6'
  end

  it 'should get result' do
    expect(SumOfDigitsDigitalRoot::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
