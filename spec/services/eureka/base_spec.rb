require 'rails_helper'

RSpec.describe 'Eureka:Base' do
  before do
    @digits = '1, 100'
    @answers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 89]
  end

  it 'should get result' do
    expect(Eureka::Base.new(@digits.to_s).calculate_result).to eq(@answers)
  end
end
