require 'rails_helper'

RSpec.describe 'NumberOfTrailingZeros:Base' do
  before do
    @number = '30'
    @answer = '7'
  end

  it 'should get result' do
    expect(NumberOfTrailingZeros::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
