require 'rails_helper'

RSpec.describe 'StringsMix:Base' do
  before do
    @string = '"looping is fun but dangerous", "less dangerous than coding"'
    @answer = "1:ooo/1:uuu/2:sss/=:nnn/1:ii/2:aa/2:dd/2:ee/=:gg"
  end

  it 'should get result' do
    expect(StringsMix::Base.new(@string).calculate_result).to eq(@answer)
  end
end
