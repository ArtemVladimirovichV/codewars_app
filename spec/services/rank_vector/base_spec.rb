require 'rails_helper'

RSpec.describe 'RankVector:Base' do
  before do
    @number = '[9, 3, 6, 10]'
    @answer = '[2, 4, 3, 1]'
  end

  it 'should get result' do
    expect(RankVector::Base.new(@number).calculate_result).to eq(@answer)
  end
end
