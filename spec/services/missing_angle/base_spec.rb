require 'rails_helper'

RSpec.describe 'MissingAngle:Base' do
  before do
    @numbers = '5, 4, 0'
    @answer = '37'
  end

  it 'should get result' do
    expect(MissingAngle::Base.new(@numbers).calculate_result.to_s).to eq(@answer)
  end
end
