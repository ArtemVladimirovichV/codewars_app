require 'rails_helper'

RSpec.describe 'TriangleType:Base' do
  before do
    @sides = '8, 5, 7'
    @answer = '1'
  end

  it 'should get result' do
    expect(TriangleType::Base.new(@sides).calculate_result.to_s).to eq(@answer)
  end
end
