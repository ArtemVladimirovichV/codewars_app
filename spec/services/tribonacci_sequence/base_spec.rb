require 'rails_helper'

RSpec.describe 'TribonacciSequence:Base' do
  before do
    @conditions = "[0, 1, 1], 10"
    @answer = "[0, 1, 1, 2, 4, 7, 13, 24, 44, 81]"
  end

  it 'should get result' do
    expect(TribonacciSequence::Base.new(@conditions).calculate_result).to eq(@answer)
  end
end
