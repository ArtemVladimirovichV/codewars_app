require 'rails_helper'

RSpec.describe 'FindTheUniqueNumber:Base' do
  before do
    @array = '[ 0, 0, 0.55, 0, 0 ]'
    @answer = '0.55'
  end

  it 'should get result' do
    expect(FindTheUniqueNumber::Base.new(@array).calculate_result.to_s).to eq(@answer)
  end
end
