require 'rails_helper'

RSpec.describe 'GenerateNumbersFromDigits2:Base' do
  before do
    @array = "['3', '7', '7', '7', '3', '3', '3', '7', '8', '8', '8',
    '8', '1', '1', '1']"
    @answer = "[64, 40, 873]"
  end

  it 'should get result' do
    expect(GenerateNumbersFromDigits2::Base.new(@array).calculate_result).to eq(@answer)
  end
end
