require 'rails_helper'

RSpec.describe 'CompareVersions:Base' do
  before do
    @number = '153'
    @answer = true
  end

  it 'should get result' do
    expect(CompareVersions::Base.new(@number).calculate_result).to eq(@answer)
  end
end
