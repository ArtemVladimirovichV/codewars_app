require 'rails_helper'

RSpec.describe 'CountTheSmileyFaces:Base' do
  before do
    @condition = '[":D",":~)",";~D",":)"]'
    @answer = '4'
  end

  it 'should get result' do
    expect(CountTheSmileyFaces::Base.new(@condition).calculate_result).to eq(@answer)
  end
end
