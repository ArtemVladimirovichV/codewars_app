require 'rails_helper'

RSpec.describe 'BackspacesInString:Base' do
  before do
    @number = 'abc#d##c'
    @answer = 'ac'
  end

  it 'should get result' do
    expect(BackspacesInString::Base.new(@number).calculate_result).to eq(@answer)
  end
end
