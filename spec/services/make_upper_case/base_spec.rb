require 'rails_helper'

RSpec.describe 'MakeUpperCase:Base' do
  before do
    @letters = 'sedwefr'
    @answers = 'SEDWEFR'
  end

  it 'should get result' do
    expect(MakeUpperCase::Base.new(@letters).calculate_result).to eq(@answers)
  end
end
