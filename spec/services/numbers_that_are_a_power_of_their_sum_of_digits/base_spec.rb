require 'rails_helper'

RSpec.describe 'NumbersThatAreAPowerOfTheirSumOfDigits:Base' do
  before do
    @number = '4'
    @answer = '4913'
  end

  it 'should get result' do
    expect(NumbersThatAreAPowerOfTheirSumOfDigits::Base.new(@number).calculate_result.to_s).to eq(@answer)
  end
end
