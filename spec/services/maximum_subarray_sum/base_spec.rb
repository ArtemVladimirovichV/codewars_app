require 'rails_helper'

RSpec.describe 'MaximumSubarraySum:Base' do
  before do
    @array = '[-2, 1, -7, 4, -10, 2, 1, 5, 4]'
    @answer = '12'
  end

  it 'should get result' do
    expect(MaximumSubarraySum::Base.new(@array).calculate_result.to_s).to eq(@answer)
  end
end
