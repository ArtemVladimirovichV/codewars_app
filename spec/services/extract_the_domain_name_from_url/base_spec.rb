require 'rails_helper'

RSpec.describe 'ExtractTheDomainNameFromUrl:Base' do
  before do
    @string = "http://google.co.jp"
    @answer = "google"
  end

  it 'should get result' do
    expect(ExtractTheDomainNameFromUrl::Base.new(@string).calculate_result).to eq(@answer)
  end
end
